package com.midgard.inventory.db.api

import com.midgard.inventory.db.domain.LocationRow
import com.midgard.inventory.dto.CreateLocationData
import scalikejdbc.async.AsyncDBSession

trait LocationRepository[F[_]] extends DBRepository {
  def create(data: CreateLocationData)(implicit dbS: AsyncDBSession): F[LocationRow]
  def findOne(id: Long)(implicit dbS: AsyncDBSession): F[LocationRow]
}
