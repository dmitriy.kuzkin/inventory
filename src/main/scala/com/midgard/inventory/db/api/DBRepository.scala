package com.midgard.inventory.db.api

import scala.concurrent.ExecutionContext

trait DBRepository {
  implicit val dbEc: ExecutionContext
}
