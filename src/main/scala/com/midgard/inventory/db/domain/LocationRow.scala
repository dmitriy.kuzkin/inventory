package com.midgard.inventory.db.domain

import scalikejdbc._

case class LocationRow(
    id: Long,
    name: String,
    code: String,
    createdAt: DBTimeInSecond
)

object LocationRow extends SQLSyntaxSupport[LocationRow] {
  override def tableName: String = "inventory_location"
  override lazy val columns = autoColumns[LocationRow]()
  def apply(rs: WrappedResultSet, rn: ResultName[LocationRow]): LocationRow =
    autoConstruct(rs, rn)
}
