package com.midgard.inventory.db.domain

import java.sql.ResultSet
import java.time.{Instant, LocalDateTime, ZoneOffset}

import scalikejdbc.{ParameterBinderFactory, TypeBinder}

case class DBTimeInSecond(value: Long)

object DBTimeInSecond {
  implicit val DBTimeInSecondTB: TypeBinder[DBTimeInSecond] = new TypeBinder[DBTimeInSecond] {
    def apply(rs: ResultSet, label: String): DBTimeInSecond = DBTimeInSecond(rs.getLong(label))
    def apply(rs: ResultSet, index: Int): DBTimeInSecond = DBTimeInSecond(rs.getLong(index))
  }
  implicit val DBTimeInSecondPdb = ParameterBinderFactory[DBTimeInSecond] { value => (stmt, idx) =>
    stmt.setLong(idx, value.value)
  }
  implicit val toLocalDateTime: DBTimeInSecond => LocalDateTime = dbTime =>
    Instant.ofEpochSecond(dbTime.value).atOffset(ZoneOffset.UTC).toLocalDateTime
}
