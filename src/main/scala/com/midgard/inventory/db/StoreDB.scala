package com.midgard.inventory.db

import scalikejdbc.async.{AsyncConnectionPool, AsyncDB, TxAsyncDBSession}

import scala.concurrent.Future

class StoreDB {
  // implement retry here
  def localTx[A](op: TxAsyncDBSession => Future[A]): Future[A] = AsyncDB.localTx(op)
}

trait StoreDBComponents {
  // todo just don't care now
  AsyncConnectionPool.singleton("jdbc:postgresql://localhost:5432/inventory", "sa", "sa")
}
