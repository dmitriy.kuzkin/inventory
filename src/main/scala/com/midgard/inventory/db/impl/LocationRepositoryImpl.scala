package com.midgard.inventory.db.impl

import java.time.{LocalDateTime, ZoneOffset}

import cats.data.OptionT
import cats.implicits._
import com.midgard.inventory.db.api.LocationRepository
import com.midgard.inventory.db.domain.{DBTimeInSecond, LocationRow}
import com.midgard.inventory.dto.CreateLocationData
import com.midgard.inventory.error.LocationNotFoundById
import scalikejdbc._
import scalikejdbc.async.AsyncDBSession
import scalikejdbc.async.FutureImplicits._

import scala.concurrent._

class LocationRepositoryImpl()(implicit override val dbEc: ExecutionContext) extends LocationRepository[Future] {

  override def create(data: CreateLocationData)(implicit dbS: AsyncDBSession): Future[LocationRow] = {
    val c = LocationRow.column
    val insertAndGetId = for {
      id <- withSQL {
        insertInto(LocationRow)
          .namedValues(
            c.name      -> data.name,
            c.code      -> data.code,
            c.createdAt -> DBTimeInSecond(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC))
          )
          .returningId
      }.updateAndReturnGeneratedKey()
    } yield id
    for {
      id  <- insertAndGetId
      res <- findOne(id)
    } yield res
  }

  override def findOne(id: Long)(implicit dbS: AsyncDBSession): Future[LocationRow] =
    OptionT(find(id)).getOrElse(throw LocationNotFoundById(id))

  private def find(id: Long)(implicit dbS: AsyncDBSession): Future[Option[LocationRow]] = {
    val c = LocationRow.syntax
    withSQL {
      selectFrom(LocationRow as c).where.eq(c.id, id)
    }.map(rs => LocationRow(rs, c.resultName))
  }
}
