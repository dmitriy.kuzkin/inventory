package com.midgard.inventory.error

sealed trait DomainError extends RuntimeException { val error: String }

final case class LocationNotFoundById(id: Long) extends DomainError {
  override val error: String = s"Location with id:`$id` not exists"
}
