package com.midgard.inventory

import java.util.concurrent.Executors.newFixedThreadPool

import cats.implicits._
import com.midgard.inventory.converter.LocationConverter
import com.midgard.inventory.db.{StoreDB, StoreDBComponents}
import com.midgard.inventory.db.impl.LocationRepositoryImpl
import com.midgard.inventory.dto.CreateLocationData
import com.midgard.inventory.service.LocationService

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

object TestBoot extends App with StoreDBComponents{

  private val db = new StoreDB()
  private val dbEc = ExecutionContext.fromExecutor(newFixedThreadPool(2 * Runtime.getRuntime.availableProcessors()))
  implicit private val ec = scala.concurrent.ExecutionContext.Implicits.global // https://github.com/typelevel/cats/issues/670
  private val locationRepository = new LocationRepositoryImpl()(dbEc)
  private val locationConverter = LocationConverter.converterF
  private val service = new LocationService(locationRepository, locationConverter)
  val c = Await.result(
    db.localTx { implicit dbS =>
      service.createLocation(CreateLocationData("loc-1", "name-1"))
    },
    Duration.Inf
  )
  println(c)

}
