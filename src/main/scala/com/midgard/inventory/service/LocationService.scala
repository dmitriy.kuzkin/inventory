package com.midgard.inventory.service

import cats.Monad
import cats.syntax.all._
import com.midgard.inventory.converter.LocationConverter
import com.midgard.inventory.db.api.LocationRepository
import com.midgard.inventory.dto.{CreateLocationData, LocationData}
import scalikejdbc.async.AsyncDBSession

class LocationService[M[_]: Monad](
    locationRepo: LocationRepository[M],
    converter: LocationConverter[M]
) {

  def createLocation(data: CreateLocationData)(implicit dbS: AsyncDBSession): M[LocationData] = {
    for {
      raw <- locationRepo.create(data)
      res <- converter.toLocationData(raw)
    } yield res
  }
}
