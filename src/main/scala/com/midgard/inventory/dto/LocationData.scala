package com.midgard.inventory.dto

import java.time.LocalDateTime

case class CreateLocationData(name: String, code: String)
case class LocationData(id: Long, name: String, code: String, createdAt: LocalDateTime)
