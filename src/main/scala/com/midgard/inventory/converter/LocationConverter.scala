package com.midgard.inventory.converter

import com.midgard.inventory.db.domain.LocationRow
import com.midgard.inventory.dto.LocationData

import scala.concurrent.Future

trait LocationConverter[F[_]] {
  def toLocationData(row: LocationRow): F[LocationData]
}

object LocationConverter {
  val converterF = new LocationConverter[Future] {
    override def toLocationData(row: LocationRow): Future[LocationData] =
      Future.successful(LocationData(id = row.id, name = row.name, code = row.code, createdAt = row.createdAt))
  }
}
