create table inventory_location
(
    id         serial,
    name       varchar(255),
    code       varchar(255),
    created_at bigint
);