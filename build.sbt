scalaVersion in ThisBuild := "2.13.1"

libraryDependencies ++= Seq(
  "org.scalikejdbc"       %% "scalikejdbc-async"                % "0.12.1",
  "org.scalikejdbc"       %% "scalikejdbc-syntax-support-macro" % "3.4.0",
  "com.github.jasync-sql" % "jasync-postgresql"                 % "1.0.5",
  "org.typelevel"         %% "cats-core"                        % "2.0.0",
  "org.typelevel"         %% "cats-effect"                      % "2.0.0",
  "com.github.pureconfig" %% "pureconfig"                       % "0.12.1",
  "org.slf4j"             % "slf4j-simple"                      % "1.7.28",
  "org.scalatest"         %% "scalatest"                        % "3.0.8" % Test,
  "com.h2database"        % "h2"                                % "1.4.200" % Test
  //  "org.scalikejdbc"       %% "scalikejdbc-config"               % "3.4.0"
)
